package main

import (
	"flag"
	"fmt"
	"os"
	"strconv"
	"jeffrey/guess"
)

var (
	help bool
	generate bool
	check bool
	compare bool
)

func init() {
	flag.BoolVar(&help, "h", false, "Usage and option help")
	flag.BoolVar(&generate, "g", false, "Generate secret randomly")
	flag.BoolVar(&check, "k", false, "Check test validity")
	flag.BoolVar(&compare, "p", false, "Compare secret with test")
	flag.Parse()
}

func main() {
	if help || len(os.Args) == 1 {
		fmt.Fprintln(os.Stderr, "Usage:", os.Args[0], `[-h] [-g N] [-k "TEST"] [-p "SECRET" "TEST"]`)
		flag.PrintDefaults()
	}
	if generate {
		n, _ := strconv.Atoi(flag.Arg(0))
		fmt.Fprint(os.Stdout, guess.Generate(n))
	}
	if check {
		fmt.Fprint(os.Stdout, guess.Check([]byte(flag.Arg(0))))
	}
	if compare {
		p, c := guess.Compare([]byte(flag.Arg(0)), []byte(flag.Arg(1)))
		fmt.Fprint(os.Stdout, p, "A", c, "B")
	}
}

package guess

func Generate(n int) secret {
	if !(n >= 0 && n <= len(All)) {
		return secret{}
	}
	return sampleFrom(All, 0, n)
}

func Check(s secret) bool {
	if hasDuplicate(s) || hasInvalid(s) {
		return false
	}
	return true
}

func Compare(s, t secret) (p, c int) {
	nt := len(t)
	ns := len(s)
	if nt != ns {
		return 0, 0
	}
	for i := 0; i < nt; i++ {
		if t[i] == s[i] {
			p++
		} else {
			for j := 0; j < ns; j++ {
				if t[i] == s[j] {
					c++
				}
			}
		}
	}
	return
}

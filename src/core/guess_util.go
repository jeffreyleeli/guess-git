package guess

import (
	"io/ioutil"
	"rand"
)

func sampleFrom(d secret, s, m int) (r secret) {
	n := len(d)
	if (s+m) > n {
		return
	}
	r = make(secret, n)
	for i := 0; i < n; i++ {
		r[i] = d[i]
	}
	for i := s; i < s+m; i++ {
		t := rand.Intn(n)
		r[i], r[t] = r[t], r[i]
	}
	return r[0:m]
}

func hasDuplicate(s secret) bool {
	n := len(s)
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			if (i != j) && (s[i] == s[j]) {
				return true
			}
		}
	}
	return false
}

func hasInvalid(s secret) bool {
	n := len(s)
	for i := 0; i < n; i++ {
		if !in(s[i]) {
			return true
		}
	}
	return false
}

func in(c byte) bool {
	n := len(All)
	for i := 0; i < n; i++ {
		if All[i] == c {
			return true
		}
	}
	return false
}

func updateAllIfExist() {
	all, e := ioutil.ReadFile(home + "/.guessrc")
	if e != nil {
		return
	}
	m := len(all) - 1
	if m < 4 {
		return
	}
	All = all[0:m]
}

func (s secret) String() string {
	r := ""
	n := len(s)
	for i := 0; i < n; i++ {
		r +=string(s[i])
	}
	return r
}

package guess

import (
	"os"
	"rand"
	"time"
)

type secret []byte

var (
	home = os.Getenv("HOME")
	All = secret{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}
)

func init() {
	rand.Seed(time.Seconds())
	updateAllIfExist()
}

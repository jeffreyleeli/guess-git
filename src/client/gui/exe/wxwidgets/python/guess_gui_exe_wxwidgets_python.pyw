#!/usr/bin/python

import os
import wx
from wx import xrc

class GuessApp(wx.App):
    def OnInit(self):
        self.n = 0
        self.secret = ''
        
        res = xrc.XmlResource('/home/jeffrey/develop/bin/guess_gui_exe_wxwidgets_python.xrc')
        
        frame = res.LoadFrame(None, 'mainFrame')
        frame.Show()
        
        self.guessTextCtrl = xrc.XRCCTRL(frame, 'guessTextCtrl')
        self.hintTextCtrl = xrc.XRCCTRL(frame, 'hintTextCtrl')
        self.messageTextCtrl = xrc.XRCCTRL(frame, 'messageTextCtrl')
        
        self.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown, id=xrc.XRCID('guessTextCtrl'))
        self.Bind(wx.EVT_BUTTON, self.OnRestartButtonClicked, id=xrc.XRCID('restartButton'))
        
        self.__restart__()
        
        return True
    
    def OnKeyDown(self, event):
    	if (wx.WXK_RETURN != event.GetKeyCode()):
    		event.Skip()
    		return
    	
        guess = self.guessTextCtrl.GetValue()
        if (len(guess) != 4):
            self.messageTextCtrl.SetValue('Invalid length other than 4')
            return
        
        ok = os.popen('/home/jeffrey/develop/bin/guess-server -k ' + guess).read()
        if (ok == 'false'):
            self.messageTextCtrl.SetValue('Invalid symbols')
            return
        
        self.n += 1
        self.guessTextCtrl.Clear()
        
        hint = os.popen('/home/jeffrey/develop/bin/guess-server -p ' + self.secret + ' ' + guess).read()
        if (hint == '4A0B'):
            wx.MessageBox('That\'s it. Congratulations~:-)')
            self.__restart__()
            return
        
        ns = '{0}'.format(self.n)
        self.hintTextCtrl.AppendText(ns + ': ' + guess + ' -> ' + hint + '\n')
        self.messageTextCtrl.SetValue('Come on ' + ns + ' turns now')
    
    def OnRestartButtonClicked(self, event):
        self.__restart__()
        self.messageTextCtrl.SetValue('Game restarted')
    
    def __restart__(self):
        self.n = 0
        
        self.guessTextCtrl.Clear()
        self.hintTextCtrl.Clear()
        self.messageTextCtrl.Clear()
        
        self.secret = os.popen('/home/jeffrey/develop/bin/guess-server -g 4').read()
    
if __name__ == '__main__':
    app = GuessApp(False)
    app.MainLoop()

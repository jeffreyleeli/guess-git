#!/usr/bin/python

import os
import sys

try:
	import pygtk
	pygtk.require('2.0')
except:
	pass

try:
	import gtk
except:
	sys.exit(1)

class Guess:
	def __init__(self):
		self.n = 0
		self.guess_command = '/home/jeffrey/develop/bin/guess-server'
		
		builder = gtk.Builder()
		builder.add_from_file('/home/jeffrey/develop/bin/guess_gui_exe_gtk_python.glade')
		builder.connect_signals(self, None)

		self.history = builder.get_object('textbuffer_guess_history')
		self.hint = builder.get_object('textbuffer_guess_hint')

		self.guess = builder.get_object('entry_guess')
		self.message = builder.get_object('entry_message')
		
		self.main_window = builder.get_object('window_main')
		self.main_window.show_all()
		
		self.__restart__()

	def main(self):
		gtk.main()

	def on_entry_guess_key_press_event(self, widget, event):
		key = event.keyval
		if (key == gtk.keysyms.Return):
			test = widget.get_text()
			if (len(test) != 4):
				self.message.set_text('Not exact 4 symbols')
				return				
			status = os.popen(self.guess_command + ' -k ' + test).read()
			if (status == 'false'):
				self.message.set_text(' Out of range')
				return
			self.__try__(test)
			widget.set_text('')

	def on_button_restart_clicked(self, widget, data=None):
		self.__restart__()
		self.message.set_text('Game restarted')

	def __try__(self, test):
		self.n += 1
		self.__append_to__(self.history, test)
		hint = self.__compare__(test)
		self.__append_to__(self.hint, hint)
		self.message.set_text(self.__itoa__(self.n) + ' try come on')
		if (hint == '4A0B'):
			dialog = gtk.MessageDialog(self.main_window, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO, gtk.BUTTONS_CLOSE, 'Congratulations!')
			dialog.run()
			dialog.destroy()
			self.__restart__()

	def __compare__(self, test):
		return os.popen(self.guess_command + ' -p ' + self.secret + ' ' + test).read()

	def __append_to__(self, buffer, text):
		result_end_iter = buffer.get_end_iter()
		buffer.insert(result_end_iter, self.__itoa__(self.n) + ':' + text + '\n')

	def __restart__(self):
		self.n = 0
		
		self.guess.set_text('')
		self.message.set_text('')
		
		self.history.set_text('')
		self.hint.set_text('')
		
		self.secret = os.popen(self.guess_command + ' -g 4').read()
				
	def __itoa__(self, n):
		return '{0}'.format(n)

if __name__ == '__main__':
	guess = Guess()
	guess.main()

package main

import (
	"bytes"
	"exec"
	"fmt"
	"http"
	"os"
)

var (
	welcome = "/welcome/"
	generate = "/g/"
)

func main() {
	addServices()
	if e := startServer(); e != nil {
		fmt.Fprintln(os.Stderr, "Failed to start server:", e)
		os.Exit(1)
	}
	fmt.Println("Server shutdown successfully.")
}

func addServices() {
	http.HandleFunc(welcome, welcomeHandler)
	fmt.Println("Welcome service added successfully.")
	http.HandleFunc(generate, generateHandler)
	fmt.Println("Generate service added successfully.")
}

func welcomeHandler(c http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(c, "Hi %s, welcome to game.\n", retrieveArgsFrom(r, welcome))
}

func generateHandler(c http.ResponseWriter, r *http.Request) {
	command := "guess-server"
	option := generate[1:len(generate)-1]
	args := retrieveArgsFrom(r, generate)
	fullcommand := command + " -" + option + " " + args
	fmt.Fprintf(c, fullcommand)
	shell := "/bin/bash"
	p, err := exec.Run(shell, []string{shell, "-c", fullcommand}, nil, ".", exec.DevNull, exec.Pipe, exec.PassThrough)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to execute external command: %s\n", err)
	}
	var b bytes.Buffer
	_, e := b.ReadFrom(p.Stdout)
	if e != nil {
		fmt.Fprintf(os.Stdout, "Failed to read from p.Stdout: %s\n", e)
	}
	fmt.Println(b.String())
}

func retrieveArgsFrom(r *http.Request, site string) (args string) {
	args = r.URL.Path[len(site):]
	return
}

func startServer() (e os.Error) {
	e = http.ListenAndServe(":8080", nil)
	return
}

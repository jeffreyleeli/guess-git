package main

import (
	"bufio"
	"fmt"
	"os"
	"jeffrey/guess"
)

func main() {
	var m int = 0
	var n int = 4
	s := guess.Generate(n)
	fmt.Println("Rule: Guess", n, "distinct symbols from [", guess.All, "] and deduce each time according to hints XAYB, where X stands for number of correct position, while Y stands for number of correct symbols but not in correct position, try to solve it with minimum turns, good luck~:-)\n")
	for {
		fmt.Print("Guess: ")
		t, ok := try(n)
		if !ok {
			fmt.Println("Symbols length should be", n, ", guess again~:-(\n")
			continue
		}
		if !guess.Check(t) {
			fmt.Println("Symbols should be distinct and in range [", guess.All, "], guess again~:-(\n")
			continue
		}
		m++
		p, c := guess.Compare(s, t)
		fmt.Print("Hints: ", p, "A", c, "B", "\n\n")
		if p == n {
			break
		}
	}
	fmt.Println("Success with", m, "turns to guess [", s, "]~:-)")
}

func try(n int) (t []byte, ok bool) {
	r := bufio.NewReader(os.Stdin)
	b, _:= r.ReadBytes('\n')
	s := string(b[0:len(b)-1])
	if len(s) != n {
		return []byte{}, false
	}
	return []byte(s), true
}
